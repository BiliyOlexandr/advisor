import 'package:advisor/api/film_entity.dart';
import 'package:advisor/api/tv_entity.dart';
import 'package:advisor/ui/pages/genres_page.dart';

class ScreenArguments {
  final TypeGenre type;
  final int category;
  FilmEntity filmEntity;
  TvEntity tvEntity;

  ScreenArguments(this.type, this.category, this.filmEntity, this.tvEntity);
}