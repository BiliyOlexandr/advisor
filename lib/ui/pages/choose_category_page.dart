import 'package:advisor/constants/constants.dart';
import 'package:advisor/ui/components/type_cell.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChooseCategoryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ChooseCategoryPageState();
  }
}

class ChooseCategoryPageState extends State<ChooseCategoryPage> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: ColorConstants.backgroundColor,
      body: Center(
        child: Padding(
          padding:
              const EdgeInsets.only(top: 50, bottom: 20, left: 20, right: 20),
          child: Column(
            children: <Widget>[
              TypeCell(
                screenSize: screenSize,
                categoryImage: 'resourses/images/film.png',
                buttontitle: 'Фильми',
                onPressed: () {
                  Navigator.pushNamed(
                      context, RouteConstants.movies);
                },
              ),
              Container(
                height: 20,
              ),
              TypeCell(
                screenSize: screenSize,
                categoryImage: 'resourses/images/serials.png',
                buttontitle: 'Сериалы',
                onPressed: () {
                  Navigator.pushNamed(
                      context, RouteConstants.books);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
