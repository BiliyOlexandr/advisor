import 'package:advisor/api/advisor_client.dart';
import 'package:advisor/api/api_responce.dart';
import 'package:advisor/api/film_entity.dart';
import 'package:advisor/api/tv_entity.dart';
import 'package:advisor/constants/constants.dart';
import 'package:advisor/managers/ad_manager.dart';
import 'package:advisor/ui/components/type_cell.dart';
import 'package:advisor/ui/pages/genres_page.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../screen_argument.dart';

class RandomPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StateRandomPage();
  }
}

class StateRandomPage extends State<RandomPage> {
  String title = 'У меня сегодня';
  FilmEntity filmEntity;
  TvEntity tvEntity;
  AdvisorClient client = AdvisorClient();

  InterstitialAd _interstitialAd;

  bool _isInterstitialAdReady;

  bool firstLoad = true;
  bool isDialogShow = false;



  @override
  void initState() {
    super.initState();
    _isInterstitialAdReady = false;
    _interstitialAd = InterstitialAd(
      adUnitId: AdManager.interstitialAdUnitId,
      listener: _onInterstitialAdEvent,
    );
    _loadInterstitialAd();
    Future.delayed(Duration(seconds: 2)).then((value){
      AdManager().numberOfLoadsContents += 1;
      if (_isInterstitialAdReady && AdManager().numberOfLoadsContents % 2 == 0) {
        _interstitialAd.show();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _interstitialAd?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;
    if (filmEntity == null && args.type == TypeGenre.film) {
      _getFilm(args.category, firstLoad: firstLoad);
    } else if (tvEntity == null && args.type == TypeGenre.serials) {
      _getSerial(args.category, firstLoad: firstLoad);
    }
    firstLoad = false;
    return Scaffold(
      appBar: AppBar(
        title: Text(title,
            style: TextStyle(
              color: ColorConstants.appBarTextColor,
            )),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ColorConstants.backgroundColor,
      ),
      body: (filmEntity == null && args.type == TypeGenre.film || tvEntity == null && args.type == TypeGenre.serials)
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AspectRatio(
                      aspectRatio: 16 / 9,
                      child: Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                  (args.type == TypeGenre.film && filmEntity.backdrop.isEmpty || args.type == TypeGenre.serials && tvEntity.backdrop.isEmpty )
                                      ? DefaultValues.noImage
                                      : (args.type == TypeGenre.film)? filmEntity.backdrop: tvEntity.backdrop,
                                  scale: 0.1,
                                )),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            color: Colors.white,
                          ),
                          child: Container()),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Center(
                    child: Text(
                      (args.type == TypeGenre.film)? filmEntity.title + ' / ' + filmEntity.originalTitle: tvEntity.name + ' / ' + tvEntity.originalName,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
                MaterialButton(
                  onPressed: () async {
                    args.filmEntity = filmEntity;
                    args.tvEntity = tvEntity;
                    var received = await Navigator.pushNamed(
                        context, RouteConstants.description,
                        arguments: args);
                    if (received != null) {
                      setState(() {
                        filmEntity = null;
                        tvEntity = null;
                      });
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      width: screenSize.width * 0.85,
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          Container(
                            height: 42,
                            decoration: standartDecoration(
                                ColorConstants.backgroundColor),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: screenSize.width * 0.85 - 55,
                                  child: Center(
                                    child: Text(
                                      'Подробнее',
                                      style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 79, 79, 79)),
                                    ),
                                  ),
                                ),
                                Image.asset(
                                  'resourses/images/arrow.png',
                                  width: 50,
                                  height: 50,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                MaterialButton(
                  onPressed: () {
                    filmEntity = null;
                    tvEntity = null;
                    setState(() {});
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Container(
                      width: screenSize.width * 0.85,
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          Container(
                            height: 45,
                            decoration: standartDecoration(
                                ColorConstants.blueBackground),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: screenSize.width * 0.85 - 55,
                                  child: Center(
                                    child: Text(
                                      (args.type == TypeGenre.film)? 'Ещё фильмы': 'Ещё сериалы',
                                      style: TextStyle(
                                          color: Color.fromARGB(
                                              255, 255, 255, 255)),
                                    ),
                                  ),
                                ),
                                Image.asset(
                                  'resourses/images/shuffle.png',
                                  width: 50,
                                  height: 50,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  void _loadInterstitialAd() {
    _interstitialAd.load();
  }

  void _onInterstitialAdEvent(MobileAdEvent event) {
    switch (event) {
      case MobileAdEvent.loaded:
        _isInterstitialAdReady = true;
        break;
      case MobileAdEvent.failedToLoad:
        _isInterstitialAdReady = false;
        print('Failed to load an interstitial ad');
        break;
      case MobileAdEvent.closed:
        _interstitialAd = InterstitialAd(
          adUnitId: AdManager.interstitialAdUnitId,
          listener: _onInterstitialAdEvent,
        );
        _loadInterstitialAd();
        break;
      default:
      // do nothing
    }
  }

  _getSerial(int genreId, {bool firstLoad = false}) {
    if (!firstLoad) {
      AdManager().numberOfLoadsContents += 1;
      if (_isInterstitialAdReady &&
          AdManager().numberOfLoadsContents % 2 == 0) {
        _interstitialAd.show();
      }
    }
    if (_isInterstitialAdReady) {
      _loadInterstitialAd();
    }
    client.getSerialByGenre(genreId).then((value) {
      switch (value.status) {
        case Status.LOADING:
          break;
        case Status.COMPLETED:
          setState(() {
            tvEntity = value.data;
          });
          break;
        case Status.ERROR:
          _showDialog(value.message, genreId);
          break;
      }
    });
  }

  void _getFilm(int genreId, {bool firstLoad = false}) async {
    if (!firstLoad) {
      AdManager().numberOfLoadsContents += 1;
      if (_isInterstitialAdReady &&
          AdManager().numberOfLoadsContents % 2 == 0) {
        _interstitialAd.show();
      }
    }
    if (_isInterstitialAdReady) {
      _loadInterstitialAd();
    }
    client.getFilmByGenre(genreId).then((value) {
      switch (value.status) {
        case Status.LOADING:
          break;
        case Status.COMPLETED:
          setState(() {
            filmEntity = value.data;
          });
          break;
        case Status.ERROR:
          _showDialog(value.message, genreId);
          break;
      }
    });
  }

  void _showDialog(String error, int genreId) {
    if (isDialogShow) {
      return;
    }
    isDialogShow = true;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text('Ошибка'),
          content: new Text("Возникла ошибка при загрузке данных"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
                isDialogShow = false;
                _getFilm(genreId);
              },
            ),
          ],
        );
      },
    );
  }
}
