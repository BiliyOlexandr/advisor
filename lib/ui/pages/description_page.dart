import 'package:advisor/api/advisor_client.dart';
import 'package:advisor/api/api_responce.dart';
import 'package:advisor/api/film_entity.dart';
import 'package:advisor/api/tv_entity.dart';
import 'package:advisor/constants/constants.dart';
import 'package:advisor/ui/components/type_cell.dart';
import 'package:advisor/ui/pages/genres_page.dart';
import 'package:advisor/ui/screen_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class DescriptionPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StateDescriptionPage();
  }
}

class StateDescriptionPage extends State<DescriptionPage> {
  AdvisorClient client = AdvisorClient();
  FilmEntity filmEntity;
  TvEntity tvEntity;

  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;
    filmEntity = args.filmEntity;
    tvEntity = args.tvEntity;
    var screenSize = MediaQuery.of(context).size;
    final allGenres = (args.type == TypeGenre.film)
        ? filmEntity.genres.reduce((value, element) => value + ', ' + element)
        : tvEntity.genres.reduce((value, element) => value + ', ' + element);
    final mainRoles = (args.type == TypeGenre.film)
        ? (!filmEntity.cast[0].isEmpty)? filmEntity.cast[0].reduce((value, element) => value + ', ' + element): ''
        : (!tvEntity.cast[0].isEmpty)? tvEntity.cast[0].reduce((value, element) => value + ', ' + element): '';
    return Scaffold(
      appBar: AppBar(
        title: Text('Описание',
            style: TextStyle(
              color: ColorConstants.appBarTextColor,
            )),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share, color: Colors.black),
            onPressed: () => Share.share(
                'Я думаю тебе понравится! Рекомендую посмотреть ${(args.type == TypeGenre.film)? args.filmEntity.title: args.tvEntity.name} ${(args.type == TypeGenre.film)? args.filmEntity.releaseDate: args.tvEntity.firstAirDate + '-' + args.tvEntity.lastAirDate }. '
                    'Нашел с помощью приложения Advisor App Store - https://apps.apple.com/us/app/id1520448630, Google Play - *****'),
          )
        ],
        backgroundColor: ColorConstants.backgroundColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: AspectRatio(
                  aspectRatio: 2 / 1,
                  child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              (args.type == TypeGenre.film &&
                                          filmEntity.backdrop.isEmpty ||
                                      args.type == TypeGenre.serials &&
                                          tvEntity.backdrop.isEmpty)
                                  ? DefaultValues.noImage
                                  : (args.type == TypeGenre.film)
                                      ? filmEntity.backdrop
                                      : tvEntity.backdrop,
                              scale: 0.1,
                            )),
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        color: Colors.white,
                      ),
                      child: Container()),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(4),
              child: Center(
                child: Text(
                  (args.type == TypeGenre.film)
                      ? filmEntity.title + ' / ' + filmEntity.originalTitle
                      : tvEntity.name + ' / ' + tvEntity.originalName,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                    child: RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(
                        text: '',
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Жанр: ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.black)),
                          TextSpan(
                              text: allGenres,
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15,
                                  color: ColorConstants.textColor)),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                    child: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: '',
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Рейтинг: ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.black)),
                          TextSpan(
                              text: (args.type == TypeGenre.film)
                                  ? '${filmEntity.voteAverage}'
                                  : '${tvEntity.voteAverage}',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15,
                                  color: ColorConstants.textColor)),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                    child: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: '',
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Год выхода: ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.black)),
                          TextSpan(
                              text: (args.type == TypeGenre.film)
                                  ? '${filmEntity.releaseDate}'
                                  : '${tvEntity.firstAirDate}',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15,
                                  color: ColorConstants.textColor)),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            (args.type == TypeGenre.film)
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                          child: RichText(
                            text: TextSpan(
                              text: '',
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Режисёр: ',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                        color: Colors.black)),
                                TextSpan(
                                    text: filmEntity.director,
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 15,
                                        color: ColorConstants.textColor)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                          child: RichText(
                            text: TextSpan(
                              text: '',
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Год окончания: ',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                        color: Colors.black)),
                                TextSpan(
                                    text: tvEntity.lastAirDate,
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 15,
                                        color: ColorConstants.textColor)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                    child: RichText(
                      text: TextSpan(
                        text: '',
                        children: <TextSpan>[
                          TextSpan(
                              text: 'В главных ролях: ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.black)),
                          TextSpan(
                              text: mainRoles,
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15,
                                  color: ColorConstants.textColor)),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                (args.type == TypeGenre.film)
                    ? filmEntity.overview
                    : tvEntity.overview,
                style: new TextStyle(
                  fontSize: 15.0,
                  color: ColorConstants.textColorDart,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Row(
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      Navigator.pushNamed(context, RouteConstants.thankYouPage,
                          arguments: args);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: Container(
                        width: screenSize.width * 0.5 - 35,
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Container(
                              height: 42,
                              decoration: standartDecoration(
                                  ColorConstants.backgroundColor),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 4),
                                        child: Text(
                                          'То, что хотел!',
                                          style: TextStyle(
                                              color: Color.fromARGB(
                                                  255, 79, 79, 79)),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Image.asset(
                                    'resourses/images/arrow.png',
                                    width: 40,
                                    height: 40,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 0),
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.pop(context, "Back");
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: Container(
                          width: screenSize.width * 0.5 - 35,
                          child: Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Container(
                                height: 45,
                                decoration: standartDecoration(
                                    ColorConstants.blueBackground),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.only(left: 4),
                                          child: Text(
                                            (args.type == TypeGenre.film)? 'Ещё фильмы': 'Ещё сериалы',
                                            style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 255, 255, 255)),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Image.asset(
                                      'resourses/images/shuffle.png',
                                      width: 50,
                                      height: 50,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
