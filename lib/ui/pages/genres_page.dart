import 'package:advisor/api/advisor_client.dart';
import 'package:advisor/api/api_responce.dart';
import 'package:advisor/api/genre.dart';
import 'package:advisor/constants/constants.dart';
import 'package:advisor/ui/components/type_cell.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../screen_argument.dart';

enum TypeGenre { serials, film }

class GenresPage extends StatefulWidget {
  final TypeGenre type;

  GenresPage(this.type);

  @override
  State<StatefulWidget> createState() {
    return StateGenresPage();
  }
}

class StateGenresPage extends State<GenresPage> {
  List<Genre> genres = [];
  AdvisorClient client = AdvisorClient();
  bool isDialogShow = false;

  @override
  void initState() {
    super.initState();
    _getMovies();
  }

  void _getMovies() {
    if (widget.type == TypeGenre.film) {
      client.getFilmsGenres().then((value) {
        switch (value.status) {
          case Status.LOADING:
            break;
          case Status.COMPLETED:
            setState(() {
              genres = value.data;
            });
            break;
          case Status.ERROR:
            _showDialog("Виникла помилка при загрузкі даних");
            break;
        }
      });
    } else {
      client.getSerialGenres().then((value) {
        switch (value.status) {
          case Status.LOADING:
            break;
          case Status.COMPLETED:
            setState(() {
              genres = value.data;
            });
            break;
          case Status.ERROR:
            _showDialog(value.message);
            break;
        }
      });
    }
  }

  void _showDialog(String error) {
    if (isDialogShow) {
      return;
    }
    isDialogShow = true;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text('Ошибка'),
          content: new Text("Возникла ошибка при загрузке данных"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
                isDialogShow = false;
                _getMovies();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.backgroundColor,
      appBar: AppBar(
        title: Text(
            widget.type == TypeGenre.film ? 'Жанры фильмов' : 'Жанры сериалов',
            style: TextStyle(
              color: ColorConstants.appBarTextColor,
            )),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ColorConstants.backgroundColor,
      ),
      body: (genres.isEmpty)
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              shrinkWrap: true,
              padding: const EdgeInsets.all(8),
              itemCount: genres.length + 1,
              itemBuilder: (BuildContext context, int index) {
                var screenSize = MediaQuery.of(context).size;
                bool isFirst = index == 0;
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CupertinoButton(
                    padding: EdgeInsets.all(2),
                    onPressed: () {
                      Navigator.pushNamed(context, RouteConstants.random,
                          arguments: ScreenArguments(widget.type,
                              index == 0 ? null : genres[index - 1].id, null, null));
                    },
                    child: Container(
                      width: screenSize.width * 0.85,
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          Container(
                            height: 42,
                            decoration: standartDecoration(isFirst
                                ? ColorConstants.blueBackground
                                : ColorConstants.backgroundColor),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: screenSize.width * 0.85 - 55,
                                  child: Center(
                                    child: Text(
                                      isFirst
                                          ? widget.type == TypeGenre.film
                                              ? 'Случайный фильм'
                                              : 'Случайный сериал'
                                          : genres[index - 1].name,
                                      style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 79, 79, 79)),
                                    ),
                                  ),
                                ),
                                Image.asset(
                                  isFirst
                                      ? 'resourses/images/shuffle.png'
                                      : 'resourses/images/arrow.png',
                                  width: 50,
                                  height: 50,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }),
    );
  }
}
