import 'dart:ui';

import 'package:advisor/constants/constants.dart';
import 'package:advisor/ui/components/type_cell.dart';
import 'package:advisor/ui/pages/genres_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

import '../screen_argument.dart';

class ThankYouPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StateThankYouPage();
  }
}

class StateThankYouPage extends State<ThankYouPage> {
  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('resourses/images/splash_bg.png'),
              fit: BoxFit.cover),
        ),
        child: Container(
          color: Color.fromARGB(200, 239, 238, 238),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Padding(
                padding: EdgeInsets.only(top: screenSize.height / 4),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Image.asset('resourses/images/logo_splash.png'),
                    Center(
                      child: RichText(
                        text: TextSpan(
                          text: '',
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Рады помочь\n с выбором\n',
                                style: TextStyle(
                                    fontSize: 35,
                                    color: ColorConstants.textColorDart)),
                            TextSpan(
                                text:
                                    '         и сделать\n времяпровождение\n ещё более приятным! ',
                                style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    fontSize: 25,
                                    color: ColorConstants.textColor)),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: FractionalOffset.bottomCenter,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              MaterialButton(
                                onPressed: () async {
                                  Share.share(
                                      'Я думаю тебе понравится! Рекомендую посмотреть ${(args.type == TypeGenre.film)? args.filmEntity.title: args.tvEntity.name} ${(args.type == TypeGenre.film)? args.filmEntity.releaseDate: args.tvEntity.firstAirDate + '-' + args.tvEntity.lastAirDate }. '
                                          'Нашел с помощью приложения Advisor App Store - https://apps.apple.com/us/app/id1541212802, Google play - *****');
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Container(
                                    width: screenSize.width * 0.85,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Container(
                                          height: 42,
                                          decoration: standartDecoration(
                                              ColorConstants.backgroundColor),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                width: screenSize.width * 0.85 -
                                                    55,
                                                child: Center(
                                                  child: Text(
                                                    'Порекомендовать другу',
                                                    style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 79, 79, 79)),
                                                  ),
                                                ),
                                              ),
                                              Image.asset(
                                                'resourses/images/arrow.png',
                                                width: 50,
                                                height: 50,
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              MaterialButton(
                                onPressed: () {
                                  Navigator.popUntil(
                                      context,
                                      ModalRoute.withName(
                                          RouteConstants.start));
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Container(
                                    width: screenSize.width * 0.85,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Container(
                                          height: 45,
                                          decoration: standartDecoration(
                                              ColorConstants.blueBackground),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                width: screenSize.width * 0.85 -
                                                    55,
                                                child: Center(
                                                  child: Text(
                                                    'Ещё',
                                                    style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255,
                                                            255,
                                                            255,
                                                            255)),
                                                  ),
                                                ),
                                              ),
                                              Image.asset(
                                                'resourses/images/shuffle.png',
                                                width: 50,
                                                height: 50,
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
