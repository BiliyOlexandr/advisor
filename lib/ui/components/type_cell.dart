import 'package:advisor/constants/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TypeCell extends StatelessWidget {
  final Size screenSize;
  final String categoryImage;
  final String buttontitle;
  final Function onPressed;

  const TypeCell(
      {Key key,
      this.screenSize,
      this.categoryImage,
      this.buttontitle,
      this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(screenSize.width);
    return CupertinoButton(
        onPressed: onPressed,
        padding: EdgeInsets.all(0),
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: <Widget>[
            Container(
              width: screenSize.width * 0.85,
              height: screenSize.height * 0.42,
              child: Stack(alignment: Alignment.center, children: <Widget>[
                Container(
                  decoration: standartDecoration(ColorConstants.backgroundColor),
                ),
                Container(
                    width: 200, height: 200, child: Image.asset(categoryImage)),
              ]),
            ),
            Container(
              width: screenSize.width * 0.85,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    height: 42,
                    decoration: standartDecoration(ColorConstants.backgroundColor),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: screenSize.width * 0.85 - 55,
                          child: Center(
                            child: Text(
                              '         ' + buttontitle,
                              style: TextStyle(
                                  color: Color.fromARGB(255, 79, 79, 79)),
                            ),
                          ),
                        ),
                        Image.asset(
                          'resourses/images/arrow.png',
                          width: 50,
                          height: 50,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}

BoxDecoration standartDecoration(Color backgroundColor) {
  return BoxDecoration(
    border: Border.all(color: Color.fromARGB(50, 255, 255, 255)),
    color: backgroundColor,
    borderRadius: BorderRadius.only(
        topLeft: Radius.circular(10),
        topRight: Radius.circular(10),
        bottomLeft: Radius.circular(10),
        bottomRight: Radius.circular(10)),
    boxShadow: [
      BoxShadow(
        color: Color.fromARGB(125, 209, 205, 199),
        spreadRadius: 5,
        blurRadius: 5,
        offset: Offset(0, 3), // changes position of shadow
      ),
    ],
  );
}
