import 'package:advisor/constants/constants.dart';
import 'package:advisor/ui/pages/description_page.dart';
import 'package:advisor/ui/pages/random_page.dart';
import 'package:advisor/ui/pages/genres_page.dart';
import 'package:advisor/ui/pages/choose_category_page.dart';
import 'package:advisor/ui/pages/splash_page.dart';
import 'package:advisor/ui/pages/thank_you_page.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';

import 'managers/ad_manager.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseAdMob.instance.initialize(appId: AdManager.appId);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(fontFamily: 'Roboto'),
      routes: {
        RouteConstants.start: (context) => ChooseCategoryPage(),
        RouteConstants.books: (context) => GenresPage(TypeGenre.serials),
        RouteConstants.movies: (context) => GenresPage(TypeGenre.film),
        RouteConstants.random: (context) => RandomPage(),
        RouteConstants.description: (context) => DescriptionPage(),
        RouteConstants.thankYouPage: (context) => ThankYouPage()
      },
      home: SplashScreen(),
    );
  }
}