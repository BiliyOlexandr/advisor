// To parse this JSON data, do
//
//     final filmEntity = filmEntityFromJson(jsonString);

import 'dart:convert';

FilmEntity filmEntityFromJson(String str) => FilmEntity.fromJson(json.decode(str));

String filmEntityToJson(FilmEntity data) => json.encode(data.toJson());

class FilmEntity {
  FilmEntity({
    this.id,
    this.title,
    this.originalTitle,
    this.poster,
    this.backdrop,
    this.releaseDate,
    this.overview,
    this.voteAverage,
    this.cast,
    this.genres,
    this.director,
    this.country,
  });

  int id;
  String title;
  String originalTitle;
  String poster;
  String backdrop;
  String releaseDate;
  String overview;
  double voteAverage;
  List<List<String>> cast;
  List<String> genres;
  String director;
  String country;

  factory FilmEntity.fromJson(Map<String, dynamic> json) => FilmEntity(
    id: json["id"],
    title: json["title"],
    originalTitle: json["original_title"],
    poster: json["poster"],
    backdrop: json["backdrop"],
    releaseDate: json["release_date"],
    overview: json["overview"],
    voteAverage: double.parse(json["vote_average"].toString()),
    cast: List<List<String>>.from(json["cast"].map((x) => List<String>.from(x.map((x) => x)))),
    genres: List<String>.from(json["genres"].map((x) => x)),
    director: json["director"],
    country: json["country"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "original_title": originalTitle,
    "poster": poster,
    "backdrop": backdrop,
    "release_date": releaseDate,
    "overview": overview,
    "vote_average": voteAverage,
    "cast": List<dynamic>.from(cast.map((x) => List<dynamic>.from(x.map((x) => x)))),
    "genres": List<dynamic>.from(genres.map((x) => x)),
    "director": director,
    "country": country,
  };
}
