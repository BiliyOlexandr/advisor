import 'dart:convert';

TvEntity tvEntityFromJson(String str) => TvEntity.fromJson(json.decode(str));

String tvEntityToJson(TvEntity data) => json.encode(data.toJson());

class TvEntity {
  TvEntity({
    this.id,
    this.name,
    this.originalName,
    this.poster,
    this.backdrop,
    this.firstAirDate,
    this.lastAirDate,
    this.overview,
    this.genres,
    this.voteAverage,
    this.cast,
    this.country,
  });

  int id;
  String name;
  String originalName;
  String poster;
  String backdrop;
  String firstAirDate;
  String lastAirDate;
  String overview;
  List<String> genres;
  double voteAverage;
  List<List<String>> cast;
  String country;

  factory TvEntity.fromJson(Map<String, dynamic> json) => TvEntity(
    id: json["id"],
    name: json["name"],
    originalName: json["original_name"],
    poster: json["poster"],
    backdrop: json["backdrop"],
    firstAirDate: json["first_air_date"],
    lastAirDate: json["last_air_date"],
    overview: json["overview"],
    genres: List<String>.from(json["genres"].map((x) => x)),
    voteAverage: json["vote_average"].toDouble(),
    cast: List<List<String>>.from(json["cast"].map((x) => List<String>.from(x.map((x) => x)))),
    country: json["country"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "original_name": originalName,
    "poster": poster,
    "backdrop": backdrop,
    "first_air_date": firstAirDate,
    "last_air_date": lastAirDate,
    "overview": overview,
    "genres": List<dynamic>.from(genres.map((x) => x)),
    "vote_average": voteAverage,
    "cast": List<dynamic>.from(cast.map((x) => List<dynamic>.from(x.map((x) => x)))),
    "country": country,
  };
}