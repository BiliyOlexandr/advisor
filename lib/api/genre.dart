
class Genre {
  final int id;
  final String name;

  Genre.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        id = json['id'];
}