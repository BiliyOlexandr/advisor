import 'dart:convert' as convert;
import 'dart:convert';
import 'dart:io';
import 'package:advisor/api/api_responce.dart';
import 'package:advisor/api/film_entity.dart';
import 'package:advisor/api/genre.dart';
import 'package:advisor/api/tv_entity.dart';
import 'package:curl/curl.dart';
import 'package:http/http.dart' as http;

class AdvisorClient {
  final String apiURL = 'http://net.net.ua/api/';
  final String getMoviesGenresURL = 'getMoviesGenres';
  final String getTvGenres = 'getTvGenres';
  final String getRandomMovie = 'getRandomMovie';
  final String getRandomTvByGenre = 'getRandomTvByGenre';
  final String getRandomTv = 'getRandomTv';
  final String getRandomMovieByGenre = 'getRandomMovieByGenre';

  Future<ApiResponse<List<Genre>>> getFilmsGenres() async {
    var url = apiURL + getMoviesGenresURL;
    try {
      var response = await http.get(url);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      print(toCurl(response.request));
      if (response.statusCode != 200) {
        return ApiResponse.error('Invalid server responce');
      }
      var webJson = json.decode(utf8.decode(response.bodyBytes));
      var genresData = webJson['genres'] as List;
      if (genresData != null) {
          List<Genre> genres =
          genresData.map((item) => Genre.fromJson(item)).toList();
          return ApiResponse.completed(genres);
      }
      return ApiResponse.error(response.body.toString());
    } on SocketException catch (err) {
      return ApiResponse.error(err.osError.message);
    } catch (err) {
      return ApiResponse.error(err.toString());
    }
  }

  Future<ApiResponse<List<Genre>>> getSerialGenres() async {
    var url = apiURL + getTvGenres;
    try {
      var response = await http.get(url);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      print(toCurl(response.request));
      if (response.statusCode != 200) {
        return ApiResponse.error('Invalid server responce');
      }
      var webJson = json.decode(utf8.decode(response.bodyBytes));
      var genresData = webJson['genres'] as List;
      if (genresData != null) {
        List<Genre> genres =
        genresData.map((item) => Genre.fromJson(item)).toList();
        return ApiResponse.completed(genres);
      }
      return ApiResponse.error(response.body.toString());
    } on SocketException catch (err) {
      return ApiResponse.error(err.osError.message);
    } catch (err) {
      return ApiResponse.error(err.toString());
    }
  }

  Future<ApiResponse<TvEntity>> getSerialByGenre(int genreId) async {
    print('genreId = ${genreId}');
    var url = apiURL;
    if (genreId == null) {
      url += getRandomTv;
    } else {
      url += getRandomTvByGenre + '/${genreId}';
    }
    try {
      var response = await http.get(url);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      print(toCurl(response.request));
      if (response.statusCode != 200) {
        return ApiResponse.error('Invalid server responce');
      }
      var webJson = json.decode(utf8.decode(response.bodyBytes));
      if (webJson != null) {
        TvEntity film = TvEntity.fromJson(webJson);
        return ApiResponse.completed(film);
      }
      return ApiResponse.error(response.body.toString());
    } on SocketException catch (err) {
      return ApiResponse.error(err.osError.message);
    } catch (err) {
      return ApiResponse.error(err.toString());
    }
  }
  

  Future<ApiResponse<FilmEntity>> getFilmByGenre(int genreId) async {
    print('genreId = ${genreId}');
    var url = apiURL;
    if (genreId == null) {
      url += getRandomMovie;
    } else {
      url += getRandomMovieByGenre + '/${genreId}';
    }
    try {
      var response = await http.get(url);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      print(toCurl(response.request));
      if (response.statusCode != 200) {
        return ApiResponse.error('Invalid server responce');
      }
      var webJson = json.decode(utf8.decode(response.bodyBytes));
      if (webJson != null) {
        FilmEntity film = FilmEntity.fromJson(webJson);
        return ApiResponse.completed(film);
      }
      return ApiResponse.error(response.body.toString());
    } on SocketException catch (err) {
      return ApiResponse.error(err.osError.message);
    } catch (err) {
      return ApiResponse.error(err.toString());
    }
  }
}