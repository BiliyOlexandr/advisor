
import 'dart:ui';

class ColorConstants {
  static Color backgroundColor = Color.fromARGB(255, 239, 238, 238);
  static Color appBarTextColor = Color.fromARGB(255, 79, 79, 79);
  static Color blueBackground = Color.fromARGB(255, 86, 204, 242);
  static Color textColor = Color.fromARGB(255, 130, 130, 130);
  static Color textColorDart = Color.fromARGB(255, 79, 79, 79);
}

class RouteConstants {
  static String start = '/start';
  static String books = '/books';
  static String movies = '/movies';
  static String random =  '/random';
  static String description =  '/description';
  static String thankYouPage = '/thankYouPage';
}

class DefaultValues {
  static String noImage = 'https://www.freeiconspng.com/uploads/no-image-icon-11.PNG';
}