
import 'dart:io';

class AdManager {

  int numberOfLoadsContents = 0;

  static final AdManager _singleton = AdManager._internal();

  factory AdManager() {
    return _singleton;
  }

  AdManager._internal();

  static String get appId {
    if (Platform.isAndroid) {
      return "ca-app-pub-8527715892876904~8740881136";
    } else if (Platform.isIOS) {
      return "ca-app-pub-8527715892876904~8119840709";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get interstitialAdUnitId {
    if (Platform.isAndroid) {
      return "ca-app-pub-8527715892876904/6302613576";
    } else if (Platform.isIOS) {
      return "ca-app-pub-8527715892876904/2537971842";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }
}
